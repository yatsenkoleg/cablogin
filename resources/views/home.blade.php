@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!<br>
                    @if ($user->guid)
                        Вы зарегестрировались через личный кабинет<br>
                        GUID: {{$user->guid}}
                    @else
                        Вы зарегестрировались используя форму на сайте<br>
                    @endif
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
