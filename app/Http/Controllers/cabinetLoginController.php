<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use DB;
use App\User;


class cabinetLoginController extends Controller
{

  protected $cabinet_api = "https://cabinet.sumdu.edu.ua/api/";

  public function index(Request $req)
  {
    if(Auth::check()){//это уже авторизированный юзер

      return view('home');

    }else{

      if($req->input('key')) {//если в запросе есть ключ - делаем запрос на кабинет
        $auth_key = $req->input('key');
        $response = json_decode(file_get_contents($this->cabinet_api . 'getPerson?key=' . $auth_key));
        
        if ($response->status == 'OK') {//если кабинет подтвердил корректность ключа
          $person = $response->result;

          $user = DB::table('users')->where('email', $person->email)->first();
          if (!$user) {//юзера с таким мылом нет

            $user = User::create([
              'name' => $person->name,
              'email' => $person->email,
              'password' => Hash::make('password'),
              'guid' => $person->guid,
            ]);

            Auth::login($user, true);//авторизируем
          }else{//Юзер с таким мылом уже есть
            Auth::loginUsingId($user->id, true);//авторизируем
          }

          return redirect('home');

        }else{
          return redirect('login');
        }


      }else{
        return redirect('https://cabinet.sumdu.edu.ua/?goto=http://cabauth.samber.tk/');
      }

    }
  }
}
